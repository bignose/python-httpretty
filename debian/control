Source: python-httpretty
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper (>= 9),
 dh-python,
 openstack-pkg-tools,
 python-all,
 python-setuptools,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python-coverage,
 python-httplib2,
 python-mock,
 python-nose,
 python-pathlib2,
 python-rednose,
 python-requests,
 python-six (>= 1.11.0),
 python-sure,
 python-tornado,
 python-tz,
 python-urllib3,
 python3-httplib2,
 python3-mock,
 python3-nose,
 python3-rednose,
 python3-requests,
 python3-six (>= 1.11.0),
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-sphinxcontrib.websupport,
 python3-sure,
 python3-tornado,
 python3-tz,
 python3-urllib3,
Standards-Version: 4.1.0
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-httpretty
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-httpretty.git
Homepage: https://github.com/gabrielfalcao/httpretty
Testsuite: autopkgtest-pkg-python

Package: python-httpretty
Architecture: all
Depends:
 python-six (>= 1.11.0),
 python-urllib3,
 ${misc:Depends},
 ${python:Depends},
Description: HTTP client mock - Python 2.x
 Once upon a time a Python developer wanted to use a RESTful API, everything
 was fine but until the day he needed to test the code that hits the RESTful
 API: what if the API server is down? What if its content has changed ?
 .
 Don't worry, HTTPretty is here for you.
 .
 This package provides the Python 2.x module.

Package: python3-httpretty
Architecture: all
Depends:
 python3-six (>= 1.11.0),
 python3-urllib3,
 ${misc:Depends},
 ${python3:Depends},
Description: HTTP client mock - Python 3.x
 Once upon a time a Python developer wanted to use a RESTful API, everything
 was fine but until the day he needed to test the code that hits the RESTful
 API: what if the API server is down? What if its content has changed ?
 .
 Don't worry, HTTPretty is here for you.
 .
 This package provides the Python 3.x module.
